﻿/*Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
naprednih (sin, cos, log, sqrt...) operacija. */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_LV6_analiza_1
{
    public partial class Kalkulator : Form
    {
        double op1, op2;
        public Kalkulator()
        {
            InitializeComponent();
        }

        private void button_oduzimanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(txtB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                lbl_rez.Text = (op1 - op2).ToString();
            }
        }

        private void button_dijeljenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(txtB_op2.Text, out op2) || op2 == 0)
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                lbl_rez.Text = (op1 / op2).ToString();
            }
        }

        private void button_mnozenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(txtB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                lbl_rez.Text = (op1 * op2).ToString();
            }
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else
            {
                lbl_rez.Text = (Math.Sin(op1 * Math.PI / 180)).ToString();
            }
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else
            {
                lbl_rez.Text = (Math.Cos(op1 * Math.PI / 180)).ToString();
            }
        }

        private void button_log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else
            {
                lbl_rez.Text = (Math.Log10(op1)).ToString();
            }
        }
        private void button_sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else
            {
                lbl_rez.Text = (Math.Sqrt(op1)).ToString();
            }
        }

        private void button_tan_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else
            {
                lbl_rez.Text = (Math.Tan(op1 * Math.PI / 180)).ToString();
            }
        }

        private void button_quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_ac_Click(object sender, EventArgs e)
        {
            txtB_op1.Clear();
            txtB_op2.Clear();
            lbl_rez.Text = "";
        }

        private void button_zbrajanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(txtB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                lbl_rez.Text = (op1 + op2).ToString();
            }
        }

       


    }
}
