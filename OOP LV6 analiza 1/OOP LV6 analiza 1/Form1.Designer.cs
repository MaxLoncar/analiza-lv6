﻿namespace OOP_LV6_analiza_1
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_zbrajanje = new System.Windows.Forms.Button();
            this.txtB_op1 = new System.Windows.Forms.TextBox();
            this.lbl_op1 = new System.Windows.Forms.Label();
            this.lbl_op2 = new System.Windows.Forms.Label();
            this.txtB_op2 = new System.Windows.Forms.TextBox();
            this.lbl_r = new System.Windows.Forms.Label();
            this.lbl_rez = new System.Windows.Forms.Label();
            this.button_oduzimanje = new System.Windows.Forms.Button();
            this.button_mnozenje = new System.Windows.Forms.Button();
            this.button_dijeljenje = new System.Windows.Forms.Button();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.button_log = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.button_tan = new System.Windows.Forms.Button();
            this.button_quit = new System.Windows.Forms.Button();
            this.button_ac = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_zbrajanje
            // 
            this.button_zbrajanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_zbrajanje.Location = new System.Drawing.Point(129, 223);
            this.button_zbrajanje.Name = "button_zbrajanje";
            this.button_zbrajanje.Size = new System.Drawing.Size(75, 40);
            this.button_zbrajanje.TabIndex = 0;
            this.button_zbrajanje.Text = "+";
            this.button_zbrajanje.UseVisualStyleBackColor = true;
            this.button_zbrajanje.Click += new System.EventHandler(this.button_zbrajanje_Click);
            // 
            // txtB_op1
            // 
            this.txtB_op1.Location = new System.Drawing.Point(101, 27);
            this.txtB_op1.Name = "txtB_op1";
            this.txtB_op1.Size = new System.Drawing.Size(60, 20);
            this.txtB_op1.TabIndex = 1;
            // 
            // lbl_op1
            // 
            this.lbl_op1.AutoSize = true;
            this.lbl_op1.Location = new System.Drawing.Point(26, 30);
            this.lbl_op1.Name = "lbl_op1";
            this.lbl_op1.Size = new System.Drawing.Size(60, 13);
            this.lbl_op1.TabIndex = 2;
            this.lbl_op1.Text = "Operand 1:";
            // 
            // lbl_op2
            // 
            this.lbl_op2.AutoSize = true;
            this.lbl_op2.Location = new System.Drawing.Point(26, 68);
            this.lbl_op2.Name = "lbl_op2";
            this.lbl_op2.Size = new System.Drawing.Size(60, 13);
            this.lbl_op2.TabIndex = 3;
            this.lbl_op2.Text = "Operand 2:";
            // 
            // txtB_op2
            // 
            this.txtB_op2.Location = new System.Drawing.Point(101, 61);
            this.txtB_op2.Name = "txtB_op2";
            this.txtB_op2.Size = new System.Drawing.Size(60, 20);
            this.txtB_op2.TabIndex = 4;
            // 
            // lbl_r
            // 
            this.lbl_r.AutoSize = true;
            this.lbl_r.Location = new System.Drawing.Point(29, 100);
            this.lbl_r.Name = "lbl_r";
            this.lbl_r.Size = new System.Drawing.Size(49, 13);
            this.lbl_r.TabIndex = 5;
            this.lbl_r.Text = "Rezultat:";
            // 
            // lbl_rez
            // 
            this.lbl_rez.AutoSize = true;
            this.lbl_rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_rez.Location = new System.Drawing.Point(98, 100);
            this.lbl_rez.Name = "lbl_rez";
            this.lbl_rez.Size = new System.Drawing.Size(0, 13);
            this.lbl_rez.TabIndex = 6;
            // 
            // button_oduzimanje
            // 
            this.button_oduzimanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_oduzimanje.Location = new System.Drawing.Point(210, 223);
            this.button_oduzimanje.Name = "button_oduzimanje";
            this.button_oduzimanje.Size = new System.Drawing.Size(75, 40);
            this.button_oduzimanje.TabIndex = 7;
            this.button_oduzimanje.Text = "-";
            this.button_oduzimanje.UseVisualStyleBackColor = true;
            this.button_oduzimanje.Click += new System.EventHandler(this.button_oduzimanje_Click);
            // 
            // button_mnozenje
            // 
            this.button_mnozenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_mnozenje.Location = new System.Drawing.Point(210, 177);
            this.button_mnozenje.Name = "button_mnozenje";
            this.button_mnozenje.Size = new System.Drawing.Size(75, 40);
            this.button_mnozenje.TabIndex = 8;
            this.button_mnozenje.Text = "X";
            this.button_mnozenje.UseVisualStyleBackColor = true;
            this.button_mnozenje.Click += new System.EventHandler(this.button_mnozenje_Click);
            // 
            // button_dijeljenje
            // 
            this.button_dijeljenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_dijeljenje.Location = new System.Drawing.Point(210, 131);
            this.button_dijeljenje.Name = "button_dijeljenje";
            this.button_dijeljenje.Size = new System.Drawing.Size(75, 40);
            this.button_dijeljenje.TabIndex = 9;
            this.button_dijeljenje.Text = "÷";
            this.button_dijeljenje.UseVisualStyleBackColor = true;
            this.button_dijeljenje.Click += new System.EventHandler(this.button_dijeljenje_Click);
            // 
            // button_sin
            // 
            this.button_sin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_sin.Location = new System.Drawing.Point(48, 131);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(75, 40);
            this.button_sin.TabIndex = 10;
            this.button_sin.Text = "sin";
            this.button_sin.UseVisualStyleBackColor = true;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_cos
            // 
            this.button_cos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_cos.Location = new System.Drawing.Point(129, 131);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(75, 40);
            this.button_cos.TabIndex = 11;
            this.button_cos.Text = "cos";
            this.button_cos.UseVisualStyleBackColor = true;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // button_log
            // 
            this.button_log.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_log.Location = new System.Drawing.Point(48, 177);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(75, 40);
            this.button_log.TabIndex = 12;
            this.button_log.Text = "log";
            this.button_log.UseVisualStyleBackColor = true;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_sqrt.Location = new System.Drawing.Point(48, 223);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(75, 40);
            this.button_sqrt.TabIndex = 13;
            this.button_sqrt.Text = "√";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // button_tan
            // 
            this.button_tan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_tan.Location = new System.Drawing.Point(129, 177);
            this.button_tan.Name = "button_tan";
            this.button_tan.Size = new System.Drawing.Size(75, 40);
            this.button_tan.TabIndex = 14;
            this.button_tan.Text = "tan";
            this.button_tan.UseVisualStyleBackColor = true;
            this.button_tan.Click += new System.EventHandler(this.button_tan_Click);
            // 
            // button_quit
            // 
            this.button_quit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_quit.Location = new System.Drawing.Point(210, 14);
            this.button_quit.Name = "button_quit";
            this.button_quit.Size = new System.Drawing.Size(75, 40);
            this.button_quit.TabIndex = 15;
            this.button_quit.Text = "Quit";
            this.button_quit.UseVisualStyleBackColor = true;
            this.button_quit.Click += new System.EventHandler(this.button_quit_Click);
            // 
            // button_ac
            // 
            this.button_ac.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_ac.Location = new System.Drawing.Point(210, 73);
            this.button_ac.Name = "button_ac";
            this.button_ac.Size = new System.Drawing.Size(75, 40);
            this.button_ac.TabIndex = 16;
            this.button_ac.Text = "AC";
            this.button_ac.UseVisualStyleBackColor = true;
            this.button_ac.Click += new System.EventHandler(this.button_ac_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 291);
            this.Controls.Add(this.button_ac);
            this.Controls.Add(this.button_quit);
            this.Controls.Add(this.button_tan);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.button_cos);
            this.Controls.Add(this.button_sin);
            this.Controls.Add(this.button_dijeljenje);
            this.Controls.Add(this.button_mnozenje);
            this.Controls.Add(this.button_oduzimanje);
            this.Controls.Add(this.lbl_rez);
            this.Controls.Add(this.lbl_r);
            this.Controls.Add(this.txtB_op2);
            this.Controls.Add(this.lbl_op2);
            this.Controls.Add(this.lbl_op1);
            this.Controls.Add(this.txtB_op1);
            this.Controls.Add(this.button_zbrajanje);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Name = "Kalkulator";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_zbrajanje;
        private System.Windows.Forms.TextBox txtB_op1;
        private System.Windows.Forms.Label lbl_op1;
        private System.Windows.Forms.Label lbl_op2;
        private System.Windows.Forms.TextBox txtB_op2;
        private System.Windows.Forms.Label lbl_r;
        private System.Windows.Forms.Label lbl_rez;
        private System.Windows.Forms.Button button_oduzimanje;
        private System.Windows.Forms.Button button_mnozenje;
        private System.Windows.Forms.Button button_dijeljenje;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Button button_tan;
        private System.Windows.Forms.Button button_quit;
        private System.Windows.Forms.Button button_ac;
    }
}

