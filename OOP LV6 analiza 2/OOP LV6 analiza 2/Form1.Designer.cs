﻿namespace OOP_LV6_analiza_2
{
    partial class Vjesalo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_rijec = new System.Windows.Forms.Label();
            this.lbl_rez = new System.Windows.Forms.Label();
            this.button_quit = new System.Windows.Forms.Button();
            this.button_unesi = new System.Windows.Forms.Button();
            this.lbl_ri = new System.Windows.Forms.Label();
            this.lbl_re = new System.Windows.Forms.Label();
            this.lbl_u = new System.Windows.Forms.Label();
            this.tb_unos = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_rijec
            // 
            this.lbl_rijec.AutoSize = true;
            this.lbl_rijec.Location = new System.Drawing.Point(161, 29);
            this.lbl_rijec.Name = "lbl_rijec";
            this.lbl_rijec.Size = new System.Drawing.Size(0, 13);
            this.lbl_rijec.TabIndex = 0;
            // 
            // lbl_rez
            // 
            this.lbl_rez.AutoSize = true;
            this.lbl_rez.Location = new System.Drawing.Point(161, 61);
            this.lbl_rez.Name = "lbl_rez";
            this.lbl_rez.Size = new System.Drawing.Size(0, 13);
            this.lbl_rez.TabIndex = 1;
            // 
            // button_quit
            // 
            this.button_quit.Location = new System.Drawing.Point(234, 56);
            this.button_quit.Name = "button_quit";
            this.button_quit.Size = new System.Drawing.Size(75, 23);
            this.button_quit.TabIndex = 2;
            this.button_quit.Text = "Quit";
            this.button_quit.UseVisualStyleBackColor = true;
            this.button_quit.Click += new System.EventHandler(this.button_quit_Click);
            // 
            // button_unesi
            // 
            this.button_unesi.Location = new System.Drawing.Point(234, 24);
            this.button_unesi.Name = "button_unesi";
            this.button_unesi.Size = new System.Drawing.Size(75, 23);
            this.button_unesi.TabIndex = 4;
            this.button_unesi.Text = "Unesi";
            this.button_unesi.UseVisualStyleBackColor = true;
            this.button_unesi.Click += new System.EventHandler(this.button_unesi_Click);
            // 
            // lbl_ri
            // 
            this.lbl_ri.AutoSize = true;
            this.lbl_ri.Location = new System.Drawing.Point(25, 29);
            this.lbl_ri.Name = "lbl_ri";
            this.lbl_ri.Size = new System.Drawing.Size(34, 13);
            this.lbl_ri.TabIndex = 5;
            this.lbl_ri.Text = "Riječ:";
            // 
            // lbl_re
            // 
            this.lbl_re.AutoSize = true;
            this.lbl_re.Location = new System.Drawing.Point(25, 61);
            this.lbl_re.Name = "lbl_re";
            this.lbl_re.Size = new System.Drawing.Size(74, 13);
            this.lbl_re.TabIndex = 6;
            this.lbl_re.Text = "Broj pokušaja:";
            // 
            // lbl_u
            // 
            this.lbl_u.AutoSize = true;
            this.lbl_u.Location = new System.Drawing.Point(28, 95);
            this.lbl_u.Name = "lbl_u";
            this.lbl_u.Size = new System.Drawing.Size(37, 13);
            this.lbl_u.TabIndex = 7;
            this.lbl_u.Text = "Unesi:";
            // 
            // tb_unos
            // 
            this.tb_unos.Location = new System.Drawing.Point(130, 95);
            this.tb_unos.Name = "tb_unos";
            this.tb_unos.Size = new System.Drawing.Size(100, 20);
            this.tb_unos.TabIndex = 8;
            // 
            // Vjesalo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 295);
            this.Controls.Add(this.tb_unos);
            this.Controls.Add(this.lbl_u);
            this.Controls.Add(this.lbl_re);
            this.Controls.Add(this.lbl_ri);
            this.Controls.Add(this.button_unesi);
            this.Controls.Add(this.button_quit);
            this.Controls.Add(this.lbl_rez);
            this.Controls.Add(this.lbl_rijec);
            this.Name = "Vjesalo";
            this.Text = "Vjesalo";
            this.Load += new System.EventHandler(this.Vjesalo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_rijec;
        private System.Windows.Forms.Label lbl_rez;
        private System.Windows.Forms.Button button_quit;
        private System.Windows.Forms.Button button_unesi;
        private System.Windows.Forms.Label lbl_ri;
        private System.Windows.Forms.Label lbl_re;
        private System.Windows.Forms.Label lbl_u;
        private System.Windows.Forms.TextBox tb_unos;
    }
}

