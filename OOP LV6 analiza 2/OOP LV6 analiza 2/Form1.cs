﻿/*Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_LV6_analiza_2
{
    public partial class Vjesalo : Form
    {
        Random rand = new Random();
        List<string> list = new List<string>();
        int br_pokusaja;
        string PojamLista, PojamLabel;

        public void Reset()
        {
            PojamLista = list[rand.Next(0, list.Count - 1)];
            PojamLabel = new string('*', PojamLista.Length);
            lbl_rijec.Text = PojamLabel;
            br_pokusaja = 7;
            lbl_rez.Text = br_pokusaja.ToString();

        }

        public Vjesalo()
        {
            InitializeComponent();
        }

        private void button_quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Vjesalo_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@"C:\Users\Max\source\repos\Analiza_LV6\OOP LV6 analiza 2\vjesala.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
                Reset();
            }
        }

        private void button_unesi_Click(object sender, EventArgs e)
        { 
            if (tb_unos.Text.Length == 1)
            {
                if (PojamLista.Contains(tb_unos.Text))
                {
                    string rijec = PojamLista;
                    while (rijec.Contains(tb_unos.Text))
                    {
                        int i = rijec.IndexOf(tb_unos.Text);
                        StringBuilder builder1 = new StringBuilder(rijec);
                        builder1[i] = '*';
                        rijec = builder1.ToString();

                        StringBuilder builder2 = new StringBuilder(PojamLabel);
                        builder2[i] = Convert.ToChar(tb_unos.Text);
                        PojamLabel = builder2.ToString();

                    }
                    lbl_rijec.Text = PojamLabel;
                    if (PojamLabel == PojamLista)
                    {
                        MessageBox.Show("Pobjedili ste!", "Čestitamo!");
                        Reset();
                    }
                }
                else
                {
                    br_pokusaja--;
                    lbl_rez.Text = br_pokusaja.ToString();
                    if (br_pokusaja < 1)
                    {
                        MessageBox.Show("Izgubili ste!", "Žao nam je!");
                        Reset();
                    }
                }
            }
            else if (tb_unos.Text.Length > 1)
            {
                if (PojamLista == tb_unos.Text)
                {
                    MessageBox.Show("Pobjedili ste!", "Čestitamo!");
                    Reset();
                }
                else
                {
                    br_pokusaja--;
                    lbl_rez.Text = br_pokusaja.ToString();
                    if (br_pokusaja < 1)
                    {
                        MessageBox.Show("Izgubili ste!", "Žao nam je!");
                        Reset();
                    }
                }
            }
           tb_unos.Clear();
        }
    }
}
